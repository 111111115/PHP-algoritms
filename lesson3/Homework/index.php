<?php
//Есть отсортированный массив. 1 число пропущено. Нужно найти его и вывести через return

$array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29, 30];

function findPass($array)
{
    $result = $array[0];
    for ($i = 1; $i < count($array); $i++) {
        if ($array[$i] != $result + 1) {
            return $array[$i] - 1;
        }
        $result = $array[$i]++;
    }

}

echo findPass($array) . PHP_EOL;


// Переделать на бинарный поиск

function binarySearch($array) {

    $left = 0;
    $right = count($array) - 1;
    $n = 0;

    while ($left <= $right) {

        $n++;

        // выбираем подмассив с элементами до текущей середины
        $slicedArray = array_slice($array, $left, $right - $left + 1);

        // Определяем длинну массива $array
        $count = count($slicedArray);
        echo "Длина массива: " . $count . PHP_EOL;

        // Если длина массива меньше или равна 5, ищем недостающий элемент через цикл for
        if ($count <= 5) {

            echo "!!!КОЛИЧЕСТВО ИТЕРАЦИЙ: " . $n . PHP_EOL;

            $result = $slicedArray[0];

            for ($i = 1; $i < $count; $i++) {
                if ($slicedArray[$i] !== $result + 1) {
                    return $slicedArray[$i] - 1;
                }
                $result = $slicedArray[$i];
            }
        }

        // Иначе определяем середину массива
        $middle = floor(($left + $right) / 2);

        echo "Проверяется элемент с индексом: $middle" . PHP_EOL;

        // Выбираем подмассив с элементами до текущей середины
        $slicedArray = array_slice($array, $left, $middle - $left + 1);

        // Определяем количество элементов в текущей половине
        $count1 = count($slicedArray);
        echo "Количество элементов проверяемой половины массива: $count1" . PHP_EOL;

        // Вычисляем сумму арифметической прогрессии, где целые числа идут по порядку без пропусков
        $sum = 0;
        for ($i = $left+1 ; $i <= $left + $count1; $i++) {
            $sum += $i;
        }
        echo "Сумма элементов массива где нет пропущенного числа: $sum" . PHP_EOL;

        // Вычисляем сумму элементов реального массива
        $sum1 = array_sum($slicedArray);
        echo "Сумма элементов массива: $sum1" . PHP_EOL;

        // Если суммы не равны, меняем правую границу, если равны - левую
        if ($sum != $sum1) {
            $right = $middle;
            echo "Правая граница стала: " . $right . PHP_EOL;
        } else {
            $left = $middle;
            echo "Левая граница стала: " . $left . PHP_EOL;
        }
    }

    echo "КОЛИЧЕСТВО ИТЕРАЦИЙ: " . $n . PHP_EOL;
    return null;
}

echo binarySearch($array) . PHP_EOL;

