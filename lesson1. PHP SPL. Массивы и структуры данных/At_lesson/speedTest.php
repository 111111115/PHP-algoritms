<?php

include 'lesson.php';


const COUNT = 10000;
const ARRAY_COUNT = 1000000;

$myStack = new MyStack(COUNT); // 0.49309 // 0.46035
$splStack = new SplStack(); //0.0017 // 0.0029

$myArray = []; //---   //0.08355
$splArray = new SplFixedArray(ARRAY_COUNT);
// 0.2465 // 0.0727
// ds

$start = microtime(true);

//for($i = 0; $i < COUNT; $i++)
//	$myStack -> push(1);

for ($i = 0; $i < ARRAY_COUNT; $i++)
	$myArray[$i] = $i;

foreach ($myArray as $value)
	$value *= 2;


echo microtime(true) - $start;