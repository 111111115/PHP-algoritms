<?php
function testForeach($array)
{
    foreach ($array as $value) {
        $value *= 2;
    }
}

function testIterator($array)
{
    $iterator = new ArrayIterator($array);
    foreach ($iterator as $value) {
        $value *= 2;
    }
}

$myArray = [];


// Генерируем данные для теста
$dataSize = 1000000; // Размер данных для сравнения
$array = range(1, $dataSize);

// Запускаем измерение времени выполнения для foreach
$start = microtime(true);
testForeach($array);
$foreachTime = microtime(true) - $start;

// Запускаем измерение времени выполнения для итератора
$start = microtime(true);
testIterator($array);
$iteratorTime = microtime(true) - $start;

// Выводим результаты
echo "Размер данных: $dataSize\n";
echo "Время выполнения с использованием foreach: $foreachTime секунд\n";
echo "Время выполнения с использованием итератора: $iteratorTime секунд\n";
?>
