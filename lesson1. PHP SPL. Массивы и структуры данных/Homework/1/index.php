<!DOCTYPE HTML>
<html>
<head>
    <!-- Стили следует включить в тег head -->
    <style>
        a {
            text-decoration: none;
            color: black;
        }

        img {
            margin-right: 10px;
        }
    </style>
</head>
<body>
<?php
// Проверяем, установлен ли параметр 'src' и присваиваем переменной значение, если установлен
$src = isset($_GET['src']) ? $_GET['src'] : '';
$ddr = dirname($src);

echo "<img src='image/file.png' width='10px'>"; // Добавил единицу измерения px к ширине
echo "<a href='?src=" . htmlspecialchars($ddr) . "'>на уровень выше ...</a><br>";

// Проверяем, задан ли путь директории
if ($src == '') {
    $dir = new DirectoryIterator("/");
} else {
    $dir = new DirectoryIterator($src);
}

echo "<br>Current directory: " . htmlspecialchars($src) . "<br>";

foreach ($dir as $item) {
    if (!$item->isDot()) {
        echo "<img src='image/file.png' width='10px'><a href='?src=" . htmlspecialchars($src . "/" . $item) .
            "'>" . htmlspecialchars($item) . "</a><br> ";
    } else {
        // . и .. уже отфильтрованы, код для них не нужен
    }
}
?>
</body>
</html>
